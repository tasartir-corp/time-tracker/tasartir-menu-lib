import { MenuItemInterface } from "../services/menu-item-interface";

export class MenuSeparator implements MenuItemInterface
{
  constructor(private order: BigInt) {
  }

  getCode(): String {
    return "";
  }

  getIcon(): String|null {
    return null;
  }

  getLabel(): String|null {
    return null;
  }

  getOrder(): BigInt {
    return this.order;
  }

  getTarget(): object|null { //NuxtLinkProps|null {
    return null;
  }
}
