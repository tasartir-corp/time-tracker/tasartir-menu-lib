import { MenuItem } from "./menu-item";
import { MenuItemInterface } from "../services/menu-item-interface";
import { MenuNestedItemInterface } from "../services/menu-nested-item-interface";

export class MenuItemGroup extends MenuItem implements MenuNestedItemInterface
{
  private items: MenuItemInterface[] = [];

  public addMenuItem(item: MenuItemInterface)
  {
    this.items.push(item);
  }

  public getItems(): MenuItemInterface[] {
    return this.items
  }

  public sortAndGetItems(): MenuItemInterface[] {
    const sortFn = (a: MenuItemInterface, b: MenuItemInterface) => Number(a.getOrder().valueOf() - b.getOrder().valueOf());
    return this.items.sort(sortFn);
  }
}
