import { MenuItemInterface } from "../services/menu-item-interface";

export class MenuItem implements MenuItemInterface
{
  constructor(
    private code: String,
    private icon: String,
    private label: String,
    private order: BigInt,
    private target: object //NuxtLinkProps
    ) {
  }

  getCode(): String {
    return this.code;
  }

  getIcon(): String {
    return this.icon;
  }

  getLabel(): String {
    return this.label;
  }

  getOrder(): BigInt {
    return this.order;
  }

  getTarget(): object|null { //NuxtLinkProps|null {
    return this.target;
  }
}
