import { defineNuxtPlugin } from '#imports'
import { MenuProvider } from "../services/menu-provider";

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.provide("menuProvider", new MenuProvider());
})
