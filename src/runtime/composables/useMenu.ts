import { useNuxtApp } from "#app";
import { Menu } from "../services/menu";

export default function (name: string): Menu {
  const nuxtApp = useNuxtApp();
  const menu = new Menu();

  nuxtApp.callHook<any>('tasartir:menu:collect', name, menu);
  nuxtApp.callHook<any>('tasartir:menu:collect:' + name, menu);
  return menu
}
