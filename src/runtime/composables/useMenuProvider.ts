import { useNuxtApp } from "#app";
import { MenuProvider } from "../services/menu-provider";

export default function (): MenuProvider {
  const nuxt = useNuxtApp();

  if (!('$menuProvider' in nuxt)) {
    nuxt.provide('menuProvider', new MenuProvider);
  }

  return nuxt.$menuProvider;
}
