export interface MenuItemInterface
{
  getCode(): String;
  getLabel(): String|null;
  getIcon(): String|null;
  getOrder(): BigInt;
  getTarget(): object|null //NuxtLinkProps|null;
}
