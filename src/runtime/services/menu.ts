import { MenuItemInterface } from "./menu-item-interface";
import { MenuNestedItemInterface } from "./menu-nested-item-interface";

export class Menu implements MenuNestedItemInterface
{
  private items: MenuItemInterface[] = [];

  public addItem(menuItem: MenuItemInterface): void {
    this.items.push(menuItem);
  }

  public getItems(): MenuItemInterface[] {
    return this.items
  }

  public sortAndGetItems(): MenuItemInterface[] {
    const sortFn = (a: MenuItemInterface, b: MenuItemInterface) => Number(a.getOrder().valueOf() - b.getOrder().valueOf());
    return this.items.sort(sortFn);
  }
}
