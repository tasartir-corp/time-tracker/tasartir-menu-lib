import { MenuItemInterface } from "./menu-item-interface";

export interface MenuNestedItemInterface
{
  getItems(): MenuItemInterface[];
  sortAndGetItems(): MenuItemInterface[];
}
