import { Menu } from "./menu";

export class MenuProvider
{
  private menus: {[key: string]: Menu} = {}

  public registerMenu(name: string): Menu
  {
    if (name in this.menus) {
      throw Error("Menu with name '" + name + "' already exists.");
    }

    return this.menus[name] = new Menu();
  }

  public getMenu(name: string): Menu
  {
    if (!(name in this.menus)) {
      throw Error("Menu with name '" + name + "' wasn't registered. Please register it first.")
    }

    return this.menus[name];
  }
}
