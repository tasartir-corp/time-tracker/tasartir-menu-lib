import {defineNuxtModule, addPlugin, createResolver, addImportsDir, addImports} from '@nuxt/kit'

export * from './runtime/lib';

export interface ModuleOptions {}

export default defineNuxtModule<ModuleOptions>({
  meta: {
    name: 'tasartir-menu-lib',
    configKey: 'tasartir:menu'
  },

  defaults: {},
  setup (options, nuxt) {
    const resolver = createResolver(import.meta.url);

    addPlugin(resolver.resolve('./runtime/plugins/plugin'));
    addImportsDir(resolver.resolve('./runtime/composables'));

    nuxt.options.build.transpile.push(resolver.resolve('./runtime'));
  }
})

